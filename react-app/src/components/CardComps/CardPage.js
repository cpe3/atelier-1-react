import React, { Component} from 'react';
import '../../lib/css/bootstrap.min.css';
import CardLayout from './CardLayout';
import Header from '../Header/Header';
import { connect } from 'react-redux';
import { Container} from 'react-bootstrap';

class CardPage extends Component {

  render() {
    
    return (
        <Container>
            <Header title={this.props.type}></Header>
            <hr/><br/>
            <CardLayout type={this.props.type}/>
            <hr/><br/>
        </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
      user: state.UserReducer.current_user
  } };

export default connect(mapStateToProps)(CardPage);
