import React, { Component } from 'react';
import CardList from './CardList';
import CardBox from './CardBox';
import { Container, Row, Col } from 'react-bootstrap';

class CardLayout extends Component {

  render() {
    return (
      <Container >
        <h3> {this.props.type} - Card List</h3>
        <Row>
          <Col >
            <CardList type={this.props.type} sm={8}></CardList>
          </Col>
          <Col className="text-center my-auto"sm={4}> 
            <CardBox type={this.props.type}></CardBox>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default CardLayout;