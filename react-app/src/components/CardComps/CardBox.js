import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import OperationObject from '../OperationObject/OperationObject';
import { connect } from 'react-redux';
import { AiOutlineHeart } from "react-icons/ai";
import { BsFillLightningFill } from "react-icons/bs";
import { AiFillLock } from "react-icons/ai";
import { FaMagic } from "react-icons/fa";

class CardBox extends Component {


    render() {

        let card = this.props.currentCard;

        if (Object.entries(card).length === 0) {
            return (
                <div> No cards </div>
            );
        }

        else {

            return (
                <Container>
                    <Card className="text-center" style={{ width: '24rem' }}>
                        <Card.Header>
                            <Row>
                                <Col>
                                    <AiOutlineHeart size={24}/>
                                    {card.hp}
                                </Col>
                                <Col>
                                    {card.name}
                                </Col>
                                <Col>
                                    {card.energy}
                                    <BsFillLightningFill size={24}/>
                                </Col>
                            </Row>
                        </Card.Header>
                        <Card.Img src={card.imgUrl} />
                        <Card.Body>
                            <Card.Text>
                                {card.description}
                            </Card.Text>
                            <Row>
                                <Col>
                                    <AiOutlineHeart size={24} />HP:{card.hp}
                                </Col>
                                <Col>
                                    Energy {card.energy}
                                    <BsFillLightningFill size={24}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <AiFillLock size={24}/>Def {card.defence}
                                </Col>
                                <Col>
                                    Attack {card.attack}<FaMagic size={24}/>
                                </Col>
                            </Row>    
                        </Card.Body>
                        <Button variant="secondary" disabled>Price : {card.price}</Button>
                        <OperationObject type={this.props.type}></OperationObject>

                    </Card>

                </Container>


            );
        }

    }
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        objectList: state.CardListReducer.valueList,
        currentCard: state.CardListReducer.currentCard
    }
};

export default connect(mapStateToProps)(CardBox);