import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaShoppingCart } from "react-icons/fa";
import Button from 'react-bootstrap/Button';
import { selectItem,setBuyList } from '../../actions';
import Table from 'react-bootstrap/Table';
import axios from 'axios';

class CardList extends Component {
  constructor(props) {
    super(props);
    this.state = {card_list: null};
    if(this.props.type === 'BUY'){ // type BUY
      axios.get('http://localhost:8082/cards_to_sell')
      .then((response) => {
        this.props.dispatch(setBuyList(response.data));
        // this.setState({card_list: response.data});
      })
      .catch((error) => {
        console.log(error);

      })
      .then((response) => {
        console.log(response);
      });
    }
  }

  handleChange = (ev) => {
    this.setState({ value: ev.target.value });
  }

  handleclick = (item) => {
    this.props.dispatch(selectItem(item));
  }

  displayList = (card_list) => {
    console.log(card_list);
    if (card_list) {
      let display = card_list.map((item) =>
        <tr key={item.id}>
          <td>{item.name}</td>
          <td>{item.name}</td>
          <td>{item.family}</td>
          <td>{item.hp}</td>
          <td>{item.energy}</td>
          <td>{item.defence}</td>
          <td>{item.attack}</td>
          <td>{item.price}</td>
          <td><Button onClick={() => { this.handleclick(item) }} ><FaShoppingCart /></Button></td>
        </tr>
      )
      return (
        <div>
          <Table striped bordered hover variant="dark" size="sm">
            <thead >
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Family</th>
                <th scope="col">HP</th>
                <th scope="col">Energy</th>
                <th scope="col">Defence</th>
                <th scope="col">Attack</th>
                <th scope="col">Price</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {display}
            </tbody>
          </Table>
        </div>
      );
    }
    else {
      return (
        <div><h4>No Cards to {this.props.type}</h4></div>
      )
    }
  }

  render() {
    let card_list = null;
    if (this.props.type === 'SELL') {
      card_list = this.props.user.cardList;
    }
    else {
      card_list = this.props.buyList;
    }
    return this.displayList(card_list);
  }
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
  return {
    user: state.UserReducer.current_user,
    buyList: state.CardListReducer.buyCardList
  }
};

export default connect(mapStateToProps)(CardList);