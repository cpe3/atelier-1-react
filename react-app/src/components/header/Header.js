import React, { Component } from 'react';
import { Navbar,Nav, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FaUserCircle} from "react-icons/fa";

class Header extends Component {

  render() {

    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">CardGame - {this.props.title}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link ><Link to={'/home'} className="nav-link">Home</Link></Nav.Link>
            <Nav.Link ><Link to={'/buy'} className="nav-link">Buy</Link></Nav.Link>
            <Nav.Link ><Link to={'/sell'} className="nav-link">Sell</Link></Nav.Link>
            <Nav.Link ><Link to={'/play'} className="nav-link">Play</Link></Nav.Link>
            <Nav.Link ><Link to={'/'} className="nav-link">Logout</Link></Nav.Link>
          </Nav>

        </Navbar.Collapse>
        <Navbar.Collapse >
            <Col className="text-right">
            <FaUserCircle size={64}/>
            </Col>
            <Col>
              <Row >
                <Navbar.Text className="text-right">
                  {this.props.current_user.login}
                </Navbar.Text>
              </Row>
              <Row className="text-right">
                <Navbar.Text className="float-right">
                  Money : {this.props.current_user.account} $
                </Navbar.Text>
              </Row>
            </Col>
        </Navbar.Collapse>

      </Navbar>

    );
  }
}
//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
  return {
    current_user: state.UserReducer.current_user
  }
};

export default connect(mapStateToProps)(Header);