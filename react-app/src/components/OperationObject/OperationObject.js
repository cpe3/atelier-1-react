import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { setUser,setBuyList } from '../../actions';

class OperationObject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCard: this.props.currentCard
        }
    }

    handleChange = (ev) => {
        this.setState({ value: ev.target.value });
    }

    handleclick = () => {
        //depending the type of action distance corresponding action
        if (this.props.type === 'BUY') {
            //call the store and dispatch the addItem action
            let storeOrderBuy = {
                "user_id": this.props.user.id,
                "card_id": this.props.currentCard.id
            }
            axios.post('http://localhost:8082/buy', storeOrderBuy)
                .then((res) => {
                    console.log(res);
                    if (res.data === true) {
                        alert("card buyed");
                        axios.get('http://localhost:8082/user/' + this.props.user.id)
                            .then((res) => {
                                this.props.dispatch(setUser(res.data));
                            })
                            .catch((error) => {
                                return console.error(error);
                            });
                        axios.get('http://localhost:8082/cards_to_sell')
                            .then((response) => {
                                this.props.dispatch(setBuyList(response.data));
                                // this.setState({card_list: response.data});
                            })
                            .catch((error) => {
                                console.log(error);

                            })
                            .then((response) => {
                                console.log(response);
                            });
                    }
                    else {
                        alert("not enough money");
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        if (this.props.type === 'SELL') {
            let storeOrderSell = {
                "user_id": this.props.user.id,
                "card_id": this.props.currentCard.id
            }
            //call the store and dispatch the removeItem action
            axios.post('http://localhost:8082/sell', storeOrderSell)
                .then((res) => {
                    console.log(res);
                    if (res.data === true) {
                        alert("card selled");
                        axios.get('http://localhost:8082/user/' + this.props.user.id)
                            .then((res) => {
                                this.props.dispatch(setUser(res.data));
                            })
                            .catch((error) => {
                                return console.error(error);
                            });
                    }
                    else {
                        alert("an error occurred");
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    render() {
        return (
            <Button variant="primary" onClick={this.handleclick}>{this.props.type}</Button>
        );
    }
}

//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        currentCard: state.CardListReducer.currentCard,
        user: state.UserReducer.current_user
    }
};

//connect current component to the store
// no need to map store value to local propos
export default connect(mapStateToProps)(OperationObject);