import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserList from './UserList';
import io from 'socket.io-client';
import GeneralChat from './GeneralChat';
class ChoosePlayerLayout extends Component {
  
  constructor(props) {
    super(props)
    let socket = io.connect("http://localhost:3001");
    this.state = {socket:socket};
  }

  render() {
    return (
      <Container >
        <h3> Choose player </h3>
        <Row>
          <Col >
            <UserList socket={this.state.socket}/>
          </Col>
          <Col className="text-center my-auto"sm={4}> 
            <h3> General Chat</h3>
            <GeneralChat socket={this.state.socket}/>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default ChoosePlayerLayout;