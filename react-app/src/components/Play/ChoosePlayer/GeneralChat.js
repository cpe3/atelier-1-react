import React, { Component } from 'react';
import { Container,ListGroup,Button,InputGroup,FormControl} from 'react-bootstrap';
// import io from 'socket.io-client';
class GeneralChat extends Component {

    constructor(props) {
        super(props);
        let socket = this.props.socket;
        this.state = {
            socket:socket,
            messageList:null,
            message:""
        };
        socket.on('getMessageList', (data) => {
            this.setState({messageList:data});
        })
    }

    handleOnClick = () => {
        this.state.socket.emit('newMessage',this.state.message);
    }

    render() {
        if (this.state.messageList) {
        let messageList = this.state.messageList.map((item) =><ListGroup.Item >{item}</ListGroup.Item>)
        return (
            <Container >
                <ListGroup>
                    {messageList}
                </ListGroup>
                <InputGroup className="mb-3">
                    <FormControl
                    placeholder="Recipient's username"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                    message={this.state.message}
                    onChange={e => this.setState({ message: e.target.value })}
                    type="text"
                    />
                    <InputGroup.Append>
                    <Button variant="outline-secondary" onClick={this.handleOnClick}>Send</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Container>
        );
        }
        else {
            return (
                <Container >
                    <ListGroup>
                        <ListGroup.Item >No Messages</ListGroup.Item>
                    </ListGroup>
                    <InputGroup className="mb-3">
                    <FormControl
                    placeholder="Recipient's username"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                    message={this.state.message}
                    onChange={e => this.setState({ message: e.target.value })}
                    type="text"
                    />
                    <InputGroup.Append>
                    <Button variant="outline-secondary" onClick={this.handleOnClick}>Send</Button>
                    </InputGroup.Append>
                    </InputGroup>
                </Container>
            );
        }
    }
  }
  export default GeneralChat;