import React, { Component } from 'react';
import { Container,Tab,Row,Col,ListGroup,Alert,Button} from 'react-bootstrap';
// import io from 'socket.io-client';
import { connect } from 'react-redux';

class UserList extends Component {
    

    constructor(props) {
        super(props);
        let socket = this.props.socket
        this.state = {
            socket:socket,
            userList:null,
            fightRequest:null,
            alertShow:false
        }
        socket.emit('userIn',this.props.user);
        socket.on('userConnected', (data) => {this.setState({userList:data});});
        socket.on('sendFightAlert', (data) => {
            this.setState({fightRequest:data,
                            alertShow:true});
        });
    }
    
    handleOnUserClick = (item) => {
        this.state.socket.emit('sendFightRequest',item);
    }
    acceptFight = () => {
        
    }
    refuseFight = () => {
        
    }

    setShow = (newState) => {
        this.setState({alertShow:newState})
    }
    render() {
        if (this.state.fightRequest) {
            let userList = this.state.userList.map((item) =>
                <ListGroup.Item action onClick={() =>{this.handleOnUserClick(item)}}>{item.login}</ListGroup.Item>
                )
            return (
                <Container >
                    <Tab.Container id="list-group-tabs-example" >
                        <Row>
                            <Col sm={4}>
                                <ListGroup>
                                    {userList}
                                </ListGroup>
                            </Col>
                            <Col sm={8}>
                            <Alert show={this.state.alertShow} variant="primary">
                                    <Alert.Heading>{this.state.fightRequest} Wants to Fight You !!</Alert.Heading>
                                    <p>
                                    Do you accept ? Or Do You Refuse ???
                                    </p>
                                    <hr />
                                    <div className="d-flex justify-content-end">
                                    <Button onClick={() => {this.setShow(false)}} variant="outline-success">
                                        Accept
                                    </Button>
                                    <Button onClick={() => {this.setShow(false)}} variant="outline-danger">
                                        Refuse
                                    </Button>
                                    </div>
                                </Alert>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            );
        }
        if (this.state.userList) {
            let userList = this.state.userList.map((item) =>
                <ListGroup.Item action onClick={() =>{this.handleOnUserClick(item)}}>{item.login}</ListGroup.Item>
                )
            return (
                <Container >
                    <Tab.Container id="list-group-tabs-example" >
                        <Row>
                            <Col>
                                <ListGroup>
                                    {userList}
                                </ListGroup>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            );
        }
        else {
            return (
                <Container >
                    <Tab.Container id="list-group-tabs-example" >
                        <Row>
                            <Col>
                                <ListGroup>
                                    <ListGroup.Item>No User connected</ListGroup.Item>
                                </ListGroup>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            );
        }
        
        
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        user: state.UserReducer.current_user
    }
};

export default connect(mapStateToProps)(UserList);