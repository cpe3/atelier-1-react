import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { AiOutlineHeart } from "react-icons/ai";
import { BsFillLightningFill } from "react-icons/bs";
import { AiFillLock } from "react-icons/ai";
import { FaMagic } from "react-icons/fa";

class ChosenCard extends Component {
   
    render() {

        let card = this.props.card;

            return (
                <Container>
                    <Card className="text-center" style={{ width: '16rem' }}>
                        <Card.Header>
                            <Row>
                                <Col>
                                    <AiOutlineHeart size={24}/>
                                    {card.hp}
                                </Col>
                                <Col>
                                    {card.name}
                                </Col>
                                <Col>
                                    {card.energy}
                                    <BsFillLightningFill size={24}/>
                                </Col>
                            </Row>
                        </Card.Header>
                        <Card.Img src={card.imgUrl} />
                        <Card.Body>
                            <Card.Text>
                                {card.description}
                            </Card.Text>
                            <Row>
                                <Col>
                                    <AiOutlineHeart size={24} />HP:{card.hp}
                                </Col>
                                <Col>
                                    Energy {card.energy}
                                    <BsFillLightningFill size={24}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <AiFillLock size={24}/>Def {card.defence}
                                </Col>
                                <Col>
                                    Attack {card.attack}<FaMagic size={24}/>
                                </Col>
                            </Row>    
                        </Card.Body>
                        <Button variant="secondary" disabled>Price : {card.price}</Button>

                    </Card>

                </Container>


            );
        }

    }

export default ChosenCard;