import React, { Component } from 'react';
import {Container,Col,Row,Button} from 'react-bootstrap';
import {connect } from 'react-redux';
import ChosenCard from './ChosenCard';
import {Redirect } from 'react-router-dom';
import {setDeck} from '../../../actions';

class DeckBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:null
        }
    }
    validateDeck = (deck) => {
        this.props.dispatch(setDeck(deck));
        this.setState({redirect:'/chooseplayer'})
        // send deck to node.js via socket
    }

    render() {
        let deck = this.props.deck;
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>;
        }
        if (Object(deck).length === 0) {

            return (
                <Container>
                    <Row>
                       No Cards
                    </Row>
                </Container>
                );
        } 
        else if (Object(deck).length === 4) {
            let cards = this.props.deck.map((item) =>
                 <Col className="w-25 p-3">
                     <ChosenCard card={item}></ChosenCard>
                 </Col>
            ) 
            return (
                <Container>
                    <Row>
                        {cards}
                    </Row>
                    <Row className="text-center">
                        <Col>
                            <Button variant="success" onClick={() =>{this.validateDeck(deck)}}><h1>Validate Deck</h1></Button>
                        </Col>
                    </Row>
                </Container>
            );
        }
        else {
            let cards = this.props.deck.map((item) =>
                 <Col className="w-25 p-3">
                     <ChosenCard card={item}></ChosenCard>
                 </Col>
            ) 
            return (
                <Container>
                    <Row>
                        {cards}
                    </Row>
                </Container>
            );
        }
    }
}


//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        deck: state.PlayerReducer.deck
    }
};

export default connect(mapStateToProps)(DeckBuilder);