import React, { Component } from 'react';
import '../../../lib/css/bootstrap.min.css';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import { addToDeck } from '../../../actions';


class CardSelection extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected_cards: [],
            disable: false,
            count: 4
        }
    }
    
    handleclick = (item) => {
        this.props.dispatch(addToDeck(item));
        if (this.state.count > 1) {
            this.setState({ count: this.state.count - 1 });
        }
        else {
            this.setState({ disable: true });
        }
    }


    render() {
        let user = this.props.user;
        let array_value = user.cardList;

        //const [checked, setChecked] = useState(false);

        let display = array_value.map((item) =>
            <tr key={item.id}>
                <td>{item.name}</td>
                <td>{item.name}</td>
                <td>{item.family}</td>
                <td>{item.hp}</td>
                <td>{item.energy}</td>
                <td>{item.defence}</td>
                <td>{item.attack}</td>
                <td>{item.price}</td>
                <td>
                    <Button variant="light" onClick={() => { this.handleclick(item) }} disabled={this.state.disable}>Select</Button>
                </td>
            </tr>
        )

        return (
            <div>
                <Table striped bordered hover variant="dark" size="sm">
                    <thead >
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Family</th>
                            <th scope="col">HP</th>
                            <th scope="col">Energy</th>
                            <th scope="col">Defence</th>
                            <th scope="col">Attack</th>
                            <th scope="col">Price</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {display}
                    </tbody>
                </Table>
                <div></div>
            </div>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.UserReducer.current_user
    }
};

export default connect(mapStateToProps)(CardSelection);