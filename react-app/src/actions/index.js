/*
List of action use to interact with the store via the reducers
*/

export const setBuyList=(itemList)=>{

    return {type:'SET_BUY_CARD_LIST',itemList:itemList};
} 
export const setUser=(item)=>{
    return {type:'SET_USER',item:item};
} 
export const selectItem=(item)=>{

    return {type:'SELECT_ITEM',item:item};
} 

export const setDeck=(item)=>{

    return {type:'SET_DECK',item:item};
}
export const addToDeck=(item)=>{

    return {type:'ADD_TO_DECK',item:item};
}