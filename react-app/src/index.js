import React from 'react';
import ReactDOM from 'react-dom';
import SignupPage from './pages/SignupPage';
import LoginPage from './pages/LoginPage';
import BuyPage from './pages/BuyPage';
import SellPage from './pages/SellPage';
import SelectDeckPage from './pages/SelectDeckPage';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {createStore} from 'redux';
import globalReducer from './reducers';
import { Provider } from 'react-redux';
import PlayerPage from './pages/PlayerPage';
import HomePage from './pages/HomePage';

const store = createStore(globalReducer);

ReactDOM.render(
  <>
    <Provider store={store} >
      <Router>
        <Route exact path="/" component={LoginPage}/>
        <Route path="/home" component = {HomePage} />
        <Route path="/signup" component={SignupPage} />
        <Route path="/buy" component={BuyPage} />
        <Route path="/sell" component={SellPage} />
        <Route path="/play" component={SelectDeckPage} />
        <Route path="/chooseplayer" component={PlayerPage} />
      </Router>
    </Provider>
  </>,
  document.getElementById('root')
);

