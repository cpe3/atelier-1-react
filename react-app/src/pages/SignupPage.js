import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import {Button,Form,Container} from 'react-bootstrap';
import axios from 'axios' ;

class SignupPage extends Component {

  handleOnClick = () => {

    let newUser = { "login": document.getElementById("formLogin").value, 
    "pwd":document.getElementById("formPassword").value, 
    "account": 5000, 
    "lastName":document.getElementById("formLastName").value, 
    "surName": document.getElementById("formFirstName").value ,
    "email" : document.getElementById("formBasicEmail").value
    }
    axios.post('http://localhost:8082/user/', newUser)
    .then(() => {
      alert("user added");
    }, (error) => {
      console.log(error);
    });

  }

  render() {
    return (
        <Container>
        <Form>
        <Form.Label>Create User</Form.Label>
        <Form.Group controlId="formFirstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control type="text" placeholder="Enter First name" />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="formLastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control type="text" placeholder="Enter Last Name" />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="formLogin">
          <Form.Label>Login</Form.Label>
          <Form.Control type="text" placeholder="Enter username" />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>

        <Button variant="primary"  onClick={this.handleOnClick}>
          Sign up
        </Button>
        
      </Form>
      <br/><hr/>
      <Link to="/">Sign in</Link>
      </Container>
      
      
    );
  }
}
export default SignupPage;