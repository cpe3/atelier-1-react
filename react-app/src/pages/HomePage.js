import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import Header from '../components/Header/Header';
import { GiBuyCard, GiSellCard, GiTabletopPlayers } from "react-icons/gi";

class HomePage extends Component {

  render() {
    return (
      <Container className="text-center">
        <Header title="HOME"></Header>
        <br />
        <Row ><Col><h2>Welcome to Card Game</h2></Col></Row>
        <hr />
        <Row>
          <Col>
            <Link to={'/buy'} >
              <Button variant="primary" ><GiBuyCard size={32}/><h1>Buy</h1></Button>
            </Link>
          </Col>
          <Col>
            <Link to={'/sell'} >
              <Button variant="success" ><GiSellCard size={32}/><h1>Sell</h1></Button>
            </Link>
          </Col>
          
        </Row>
        <br/>
        <Row>
          <Col>
            <Link to={'/play'} >
            <Button variant="danger"><GiTabletopPlayers size={32}/><h1>Play</h1></Button>
            </Link>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.UserReducer.current_user
  }
};

export default connect(mapStateToProps)(HomePage);