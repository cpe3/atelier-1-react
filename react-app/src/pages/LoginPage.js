import React, { Component } from 'react';
import { Container,Button, Form} from 'react-bootstrap';
import { Redirect,Link } from 'react-router-dom';
import axios from 'axios';
import {setUser,setBuyList} from '../actions';
import { connect } from 'react-redux';

class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: null,
    };
  }
 
  handleOnClick = () => {
    let username = document.getElementById("formUsername").value;
    let password = document.getElementById("formBasicPassword").value;
    let url = 'http://localhost:8082/auth?login=' + username +'&pwd=' + password;
    // must allow cross origin 
    axios({
      method: 'get',
      url: url,
    })
    .then(res => {
      if (res.data !== "") { // if user credentials are ok , the authenticated user is returned
        console.log(res.data);
        this.props.dispatch(setUser(res.data)); // sending user to store 
        axios.get('http://localhost:8082/cards_to_sell')
          .then((response) => {
            this.props.dispatch(setBuyList(response.data));
            // this.setState({card_list: response.data});
          })
          .catch((error) => {
            console.log(error);

          })
          .then((response) => {
            console.log(response);
          });
        this.setState({redirect:"/home"}); // redirecting to home page
      }
      else {
         alert("Invalid credentials");
      }
    })
    .catch(error => {
      return console.error(error);
    });
    

  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect}/>;
    }
    return (
      <Container>
        <Form>
          <Form.Group controlId="formUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" placeholder="Enter username" />
            <Form.Text className="text-muted">
              We'll never share your username with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button variant="primary" onClick={this.handleOnClick}>
            Submit
          </Button>
        </Form>
        <br/><hr/>
        <Link to="/signup">Sign up</Link>
      </Container>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
      user: state.UserReducer.current_user
  } };

export default connect(mapStateToProps)(LoginPage);