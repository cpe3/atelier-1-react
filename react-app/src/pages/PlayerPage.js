import React, { Component } from 'react';
import {Container} from 'react-bootstrap';
import ChoosePlayerLayout from '../components/Play/ChoosePlayer/ChoosePlayerLayout';
import Header from '../components/Header/Header';

class PlayerPage extends Component {

    render() {
      return (
        <Container>
            <Header title={this.props.type}></Header>
            <hr/><br/>
            <ChoosePlayerLayout/>
        </Container>
      );
    }
  }
  export default PlayerPage;