import React, { Component } from 'react';
import {Container} from 'react-bootstrap';
import CardPage from '../components/CardComps/CardPage';

class BuyPage extends Component {

    render() {
      return(
        <Container>
          <CardPage type="BUY"/>
        </Container>
      );
    }
  }
  export default BuyPage;