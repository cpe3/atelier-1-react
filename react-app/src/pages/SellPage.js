import React, { Component } from 'react';
import {Container} from 'react-bootstrap';
import CardPage from '../components/CardComps/CardPage';


class SellPage extends Component {

    render() {
      return (
        <Container>
            <CardPage type="SELL"/>
        </Container>
      );
    }
  }
  export default SellPage;