import React, { Component } from 'react';
import {Container} from 'react-bootstrap';
import CardSelection from '../components/Play/CardSelect/CardSelection';
import Header from '../components/Header/Header';
import DeckBuilder from '../components/Play/CardSelect/DeckBuilder';


class SelectDeckPage extends Component {

    render() {
      return (
        <Container>
            <Header title={this.props.type}></Header>
            <hr/><br/>
            <CardSelection/>
            <DeckBuilder/>
        </Container>
      );
    }
  }
  export default SelectDeckPage;