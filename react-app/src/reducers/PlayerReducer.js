/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const PlayerReducer= (state={deck:[]},action) => {
    console.log(action);
    switch (action.type) {
       
        case 'SET_DECK':
        return {...state,deck:action.item};

        case 'ADD_TO_DECK':
        if (state.deck.length === 0) {
            let newDeck = [];
            newDeck.push(action.item); 
            return {...state,deck:newDeck}; 
        }
        else{
            let newDeck = [];
            Object.assign(newDeck, state.deck);
            newDeck.push(action.item); 
            return {...state,deck:newDeck};
        }
        

        default:
        return state;
    }
}
export default PlayerReducer;