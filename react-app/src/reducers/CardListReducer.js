/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const CardListReducer= (state={buyCardList:[],currentCard:{},current_user:{}},action) => {
    switch (action.type) {

        case 'SET_BUY_CARD_LIST':
            return {...state,buyCardList:action.itemList};

        case 'SELECT_ITEM':
            return {...state,currentCard:action.item};
        

        default:
            return state;
    }
}
export default CardListReducer;