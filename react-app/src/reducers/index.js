
import { combineReducers } from 'redux';
import CardListReducer from './CardListReducer';
import UserReducer from './UserReducer';
import PlayerReducer from './PlayerReducer';
/*
reducer that can contains set of reducer, usefull when several reducers are used at a timeS
*/
const globalReducer = combineReducers({
    CardListReducer: CardListReducer,
    UserReducer: UserReducer,
    PlayerReducer: PlayerReducer
});
export default globalReducer;
