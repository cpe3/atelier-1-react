/*
    Define a reducer that react on different action type such as 'ADD_ITEM', 'REMOVE_ITEM', 'LOAD_ITEMS'
    the reducer has only valueList as state value
*/
const UserReducer= (state={current_user:{}},action) => {
    console.log(action);
    switch (action.type) {
       
        case 'SET_USER':
            return {...state,current_user:action.item};
        

        default:
            return state;
    }
}
export default UserReducer;